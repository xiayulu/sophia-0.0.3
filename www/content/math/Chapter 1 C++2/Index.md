# C++ 学习笔记



# 开发环境搭建

- 安装编译器：

```shell
$ sudo apt install build-essential
$ g++ --version
```

- VScode 调试

<img src="./VScode Debuger.png" alt="VScode Debuger" style="zoom: 33%;" />

安装官方c/c++插件，然后创建 `lanunch.json`就好了。

- 安装包管理器:

```shell
# https://docs.conan.io/en/latest/installation.html
pip install conan
ls -la
```



```cpp
#include <iostream>

int main()
{
    std::cout<<"hello word!"<<std::endl;
    return 0;
}
```

```python
def hello():
    print("world")
```


```js
import {hello, world} from 'heloo-world';

export default Hello =(hi)=>({ hello: 42 });

```

```superscript
import {hello, world} from 'heloo-world';

export default Hello =(hi)=>({ hello: 42 });

superif{}
superelse

```



# 类



犯过的错误：

- 声明构造函数加了 `void`
- class 与 struct 后面没加 `;`



