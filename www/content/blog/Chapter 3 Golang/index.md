求主析取范式方法1：

1. 先求 $A$ 析取范式 $A^{\prime}$
2. 若$A^{\prime}$ 的某简单合取式不含某个命题变项 $p$ 或 $\neg p$ , 则:

$$
B\Leftrightarrow B\land 1 \Leftrightarrow B\land (p \lor \neg p) \Leftrightarrow (B\land p)\lor( B \land \neg p)
$$

3. 把变成极小项的简单合取式用写成 $m_i$ 的形式。
4. 将 $m_i$ 合并，然后从小到大排列。



主析取范式与主合取范式的应用：

1. 判断两个公式是否等值
2. 判断公式的类型：重言式，矛盾式，可满足式？
3. 真与假赋值方法

:memo: 思考题：

- 永假式的主析取范式与主合取范式是什么？

- $p \land q$ 的主析取范式与主合取范式是什么？



## 全功能集

全功能集：设 $S$ 是一个联结词的集合，如果任一真值函数都可用仅含 $S$ 中的联结词的命题公式表示，则称 $S$ 为全功能集。

:star:**定理**：$\{\neg,\land, \lor \}$， $\{\neg, \land\}$，$\{\neg, \lor\}$，$\{\neg, \rightarrow\}$ ，$\{\uparrow\}$，$\{\downarrow\}$ 都是全功能集。

证明方法：

由于任何公式可以表示成主析取范式，第一个成立。

由于 $p\lor q \Leftrightarrow \neg \neg (p \lor q) \Leftrightarrow \neg (\neg p \land\neg q )$，可见 $\lor$ 可以由 $\neg, \land$ 替换。

其实任何包含全功能集的联结词集合式全功能集。



最简展开式：对主析取范式进行简化，使其包含最少的运算。

:sparkles: 最简展开式求解方法：卡诺图法。



## 推理

若 $(A_1 \land A_2\land\dots\land A_n ) \rightarrow B$ 为重言式，则称  $A_1 \land A_2\land\dots\land A_n$ 推出结论 $B$ 的推理正确，$B$ 称为 $A_1 \land A_2\land\dots\land A_n$ 的有效结论或逻辑结论。



与用 $A \Leftrightarrow B$ 表示 $A \leftrightarrow B$ 是重言式类似，用 $A \Rightarrow B$ 表示 $A \rightarrow B$ 是重言式。

:bulb:推理正确不能保证结论正确。

推理定律：

| 定律                                                         | 描述       |
| ------------------------------------------------------------ | ---------- |
| $A \Rightarrow (A\lor B)$                                     | 附加       |
| $(A\land B) \Rightarrow A$                                    | 化简       |
| $(A\rightarrow B) \land A \Rightarrow B$                      | 假言推理   |
| $(A\rightarrow B) \land \neg B \Rightarrow \neg A$            | 拒取式     |
| $(A\lor B) \land \neg A \Rightarrow B$                         | 析取三段论 |
| $(A\rightarrow B) \land (B\rightarrow C)\Rightarrow (A\rightarrow C)$ | 假言三段论 |
| $(A\leftrightarrow B) \land (A\leftrightarrow C)\Rightarrow (A\leftrightarrow C)$ | 等价三段论 |
| $(A\rightarrow B) \land (C\rightarrow D) \land (A \lor C)\Rightarrow (B\lor D)$ | 构造性两难 |

构造证明两种技巧：

1. 附加前提证明法，把结论变成前提：

$$
(A_1\land A_2\land \dots\land A_n)\rightarrow (A\rightarrow B) \Leftrightarrow (A_1\land A_2\land \dots\land A_n \land A)\rightarrow B
$$

其实：$\neg A_n \lor (\neg A \lor B)\Leftrightarrow \neg A_n \lor \neg A \lor B$

2. 归谬法：

$$
(A_1\land A_2\land \dots\land A_n \land A)\rightarrow (B) \Leftrightarrow \neg (A_1\land A_2\land \dots\land A_n \land A \land \neg B)
$$

只要 $A_1\land A_2\land \dots\land A_n \land A \land \neg B$ 是矛盾式，就能说明推理正确。



:star:【例题】

请构造证明序列：$(P\rightarrow Q\lor R)\land(Q\rightarrow \neg P)\land(S\rightarrow \neg R) \Rightarrow (P\rightarrow \neg S)$

证明：

1. 先把条件结论列出

$$
\begin{align}
H_1&:P\rightarrow Q\lor R\\
H_2&:Q\rightarrow \neg P \\
H_3&:S\rightarrow \neg R\\
C&:P\rightarrow \neg S
\end{align}
$$

2. 利用附加前提法把结论的一部分变为前提：

$$
\begin{align}
H_1&:P\rightarrow Q\lor R\\
H_2&:Q\rightarrow \neg P \\
H_3&:S\rightarrow \neg R\\
H_4&:P\\
C&:\neg S
\end{align}
$$



3. 写出序列

$$
\begin{align}
1.&\quad Q\lor R & H_4 , H_1+假言 \\
2.&\quad P\rightarrow \neg Q & H_2+等值代换\\
3.&\quad \neg Q & H_4,2+ 假言\\
4.&\quad R & 1,3+析取三段论\\
5.&\quad R\rightarrow \neg S & H_3+等值代换\\
6.&\quad \neg S & 4，5+假言
\end{align}
$$







# 一阶逻辑

一阶逻辑也称谓词逻辑。

## 基本概念

个体常项：表示具体的和特定的个体的词，常用小写的英文字 $a,b,c,\dots$母表示。

个体变项：表示抽象的，或泛指的个体，常用小写的英文字 $x,y,z,\dots$母表示。

个体域，论域：个体变相的取值范围。当无特殊说明时，个体域只宇宙所有事物，称为全总个体域。

谓词常项：表示具体性质或关系的谓词，一般用大写英文字母 $F,G,H,\dots$ 表示。

谓词变项：表示抽象或泛指的谓词。

$F(x)$： 表示个体变项 $x$ 具有性质 $F$ 。

$L(x,y)$： 表示个体变项 $x,y$ 具有关系 $L$ 。

有时也把以上两种个体变项与谓词的联合体称作谓词。

 $n$ 元谓词：含 $n$ 个个体词的谓词称为 $n$ 元谓词，用 $P(x_1,x_2,\dots,x_n)$ 表示，它是以个体变项的个体域为定义域，以 $\{0,1\}$ 为值域的函数，它不是命题。

0元谓词：不带个体变项的谓词，0元谓词常项都是命题。

量词：除了个体词和谓词，还需要表示数量的词。量词用全称量词和存在量词两种。

**全称量词**：一切，所有，用 $\forall$ 表示，$\forall x$ 表示个体域中所有的个体，$\forall x F(x)$ 表示个体域中的所有个体都具有性质 $F$。

**存在量词**：有一个，存在，用 $\exist$ 表示，$\exist x$ 表示存在个体域中的个体，$\exist x F(x)$ 表示个体域中存在有个体具有性质 $F$。

:bulb:注意所讨论的个体域，可引入特性谓词。



**谓词公式转化为命题公式**

设个体域为有限集 $D={a_1,a_2,\dots,a_n}$，则
$$
\forall xA(x) \Leftrightarrow A(a_1)\land A(a_2)\land\dots\land A(a_n)\\
\exist xA(x) \Leftrightarrow A(a_1)\lor A(a_2)\lor\dots\lor A(a_n)
$$
:warning: 量词顺序不能随便颠倒。

例如：设 $D$ 为实数集，$H:x+y=5$

那么 $\forall x \exist yH(x,y)$ 与 $\forall x \exist yH(x,y)$ 表达的含义不同，前者为真而后者为假。



## 一阶谓词逻辑公式及解释

字母表定义如下：

- 个体常项：$a,b,c,\dots,a_i,b_i,c_i\dots$
- 个体变项：$x,y,z,\dots,x_i,y_i,z_i,\dots$
- 函数符号：$f,g,h,\dots,f_i,g_i,h_i,\dots$
- 谓词符号：$F,G,H,\dots,F_i,G_i,H_i\dots$
- 量词符号：$\forall,\exist$
- 联结词符：$\neg,\land,\lor,\rightarrow,\leftrightarrow$
- 括号和逗号：$(),$



项的递归定义：

1. 个体常项和个体变项是项

2. 若 $\phi(x_1,x_2,\dots,x_n)$ 是任意n元函数，$t_1,t_2,\dots,t_n$ 是项，则 $\phi(t_1,t_2,\dots,t_n)$ 也是项

3. 只有限次使用以上两个规则生成的符号串才是项。



原子公式：

设 $\R(x_1,x_2,\dots,x_n)$ 是任意n元谓词，$t_1,t_2,\dots,t_n$ 是项，则 $\R(t_1,t_2,\dots,t_n)$ 称为原子公式。



合式公式：

1. 原子公式是合式公式
2. 若 $A$ 是合式公式，$\neg A$ 也是合式公式。
3. 若 $A,B$ 是合式公式， 则 $A\land B,A\lor B, A\rightarrow B, A\leftrightarrow B$也是合式公式。
4. 若 $A$ 是合式公式，则 $\forall xA, \exist x A$ 也是合式公式。
5. 有限次利用以上规则构成的符号串才是合式公式。

合式公式又称谓词公式，简称公式。

