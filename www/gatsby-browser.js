// custom typefaces
import "typeface-montserrat"
import "typeface-merriweather"
// normalize CSS across browsers
import "./src/normalize.css"
// custom CSS styles
import "./src/style.css"

// prisma themes for code blocks
import "prismjs/themes/prism.css"
// import "prismjs/themes/prism-solarizedlight.css"
// import "prismjs/themes/prism-okaidia.css"
import "prismjs/plugins/command-line/prism-command-line.css"

import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.min.js"