import React from 'react'
import {Link} from "gatsby";
import CourseCard from "./course-card";

export default function HomeComputer() {
  const courses = [
    {
      id: 'computer',
      title: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
      cover_url:
        "https://sophia-1303119720.cos.ap-nanjing.myqcloud.com/course-3.jpeg",
    },
    {
      id: 1,
      title: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
      cover_url:
        "https://robocrop.realpython.net/?url=https%3A//files.realpython.com/media/Python-Basics-Chapter-on-Web-Scraping_Watermarked.ad1bb89e800b.jpg&w=480&sig=ff0f74a9ce84f25d056383d0f01a64662cf87a90",
    },
    {
      id: 2,
      title: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
      cover_url:
        "https://robocrop.realpython.net/?url=https%3A//files.realpython.com/media/Python-Basics-Chapter-on-Web-Scraping_Watermarked.ad1bb89e800b.jpg&w=480&sig=ff0f74a9ce84f25d056383d0f01a64662cf87a90",
    },
    {
      id: 3,
      title: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
      cover_url:
        "https://robocrop.realpython.net/?url=https%3A//files.realpython.com/media/A-Community-Interview_Beige.0de9df3b2fae.jpg&w=480&sig=065b62cc47280cb18697b930437ac8a5409841d5",
    },
  ];

  const courseCards = courses.map((course) => (
    <CourseCard key={course.id} course={course}></CourseCard>
  ));

  return (
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className="d-flex">
            <h5 className="me-auto">Computer</h5>
            <Link to={`/roadmap/computer`}>
              <a>更多&gt;&gt;</a>
            </Link>
          </div>
          <div className="row">{courseCards}</div>
        </div>
      </div>
    </div>
  );
}
