import React from "react";
import { StaticImage } from "gatsby-plugin-image";
import { Link } from "gatsby";

export default function CourseCard({ course }) {
  return (
    <>
      <Link to={`/course/${course.id}`} className="card col p-0 mx-2">
        <img
          src={course.cover_url}
          className="card-img-top"
          width={100}
          height={200}
        ></img>
        <div className="card-body">
          <p className="card-text">{course.title}</p>
        </div>
      </Link>
    </>
  );
}
