import React from 'react'
import { StaticImage } from "gatsby-plugin-image"


export default function HomeSlide() {
  return (
    <StaticImage
      src="http://img.netbian.com/file/2020/1217/3a4ccdf21539f42d4e1aca5de6091ffd.jpg"
      layout="fullWidth"
      className="p-4 rounded"
      placeholder="blurred"
      width={40}
      height={20}
    />
  );
}

